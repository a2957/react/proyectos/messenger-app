import axios from "axios"


export const createUser = async (data) => {
    const response = await axios({
        method: "POST",
        baseURL: 'https://academlo-whats.herokuapp.com',
        url: '/api/v1/users',
        data: data
    })
    return response
}