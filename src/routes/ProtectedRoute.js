import { useContext } from "react";
import { Redirect, Route } from "react-router-dom";
import UserContext from '../context/User/UserContext'

export const ProtectedRoute = ({children, ...props}) => {

    const {user}  = useContext(UserContext)

    return user ? <Route {...props}> {children} </Route> : <Redirect to="/" />;
};
