import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from '../components/Home'
import Messenger from '../components/Messenger'
import {ProtectedRoute} from '../routes/ProtectedRoute'

function Routes() {
    return (
        <Switch>
            <ProtectedRoute path="/messenger">
                <Messenger />
            </ProtectedRoute>
            <Route path="/">
                <Home />
            </Route>
        </Switch>
    )
}

export default Routes
