import React from 'react'
import { Route } from 'react-router-dom'
import { useRouteMatch } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import { Switch } from 'react-router-dom'
import Legend from './Legend'
import Loading from './Loading'
import SignIn from './SignIn'
import SignUp from './SignUp'

function Home() {

    const {path}  = useRouteMatch()

    return (
        <div className="Container">
            <Legend/>
            <div className="Content">
                <Switch>
                    <Route path='/loading'>
                        < Loading />
                    </Route>
                    <Route path={`/signup`}>
                        <SignUp />
                    </Route>
                    <Route path={`/signin`}>
                        <SignIn />
                    </Route>
                    <Route path={path}>
                        <Redirect to={`/signin`}/>
                    </Route>
                </Switch>
            </div>
        </div>
    )
}

export default Home
