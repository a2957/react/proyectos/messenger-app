import React, { useContext } from 'react'
import UserContext from '../context/User/UserContext'
import '../css/Messenger.css'
import Chat from './Messenger/Chat'
import Messages from './Messenger/Messages'
import User from './Messenger/User'

function Messenger() {

    const {user} = useContext(UserContext)
    
    console.log(user)
    return (
        <div className="Messenger">
            <div className="Panel">
                <User />
                <Messages />
            </div>
            <Chat />
        </div>
    )
}

export default Messenger