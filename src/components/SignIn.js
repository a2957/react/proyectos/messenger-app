import React, { useContext } from 'react';
import '../css/SignIn.css';
import { useForm } from 'react-hook-form';
import { Link, useHistory} from 'react-router-dom';
import { LogIn, validateAuth } from '../config/firebase';
import UserContext from '../context/User/UserContext';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faGoogle} from '@fortawesome/free-brands-svg-icons'

function SignIn() {
    

    const {SignIn} = useContext(UserContext)
    const history = useHistory();

    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const onSubmit = async (data) => {
        reset({
            email: '',
            password: ''
        })
        await LogIn(data.email, data.password)
        const user = validateAuth()
        SignIn(user.uid)
        
        history.push('/loading')
        
    };
    console.log(errors)

    return (
        <>
        <div className="SignIn">
            <div>{}</div>
            <h1>Account Login</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="InputGroup">
                    <input type="email" placeholder="Email" {...register("email", {required: true})} />
                    <input type="password" placeholder="Password" {...register("password", {required: true})} />
                </div>
                
                <div className="forgot">
                    <Link to="#!">Forgot password?</Link>
                </div>
                <input type="submit" value="Log In"/>
            </form>
            <div className="withGoogle">
                <div className="line"></div>
                <div className="text">
                    <h4>or log in with</h4>
                </div>
                <div className="line"></div>
            </div>
            <div className="Google">
                <button onClick={() => {
                    //TODO: Generar el ingreso por Google
                }}><FontAwesomeIcon icon={faGoogle}/> <span>Google</span></button>
            </div>
            <div className="footer">
                <h4>Don't have an account?</h4>
                <button onClick={() => {
                    history.push("/signup")
                }}>Get Started!</button>
            </div>
        </div>
        
        </>
    )
}

export default SignIn
