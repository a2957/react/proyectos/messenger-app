import React, { useContext, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import UserContext from '../context/User/UserContext'
import '../css/Loading.css'

function Loading() {


    const {user} = useContext(UserContext)
    const history = useHistory();

    useEffect(() => {
        console.log(user)
        setTimeout(() => {
            history.push('/messenger')
        }, 2000);
    }, [user,history])

    return (
        <div className="Loading" >
            <div className="cssload-thecube">
                <div className="cssload-cube cssload-c1"></div>
                <div className="cssload-cube cssload-c2"></div>
                <div className="cssload-cube cssload-c4"></div>
                <div className="cssload-cube cssload-c3"></div>
            </div>
        </div>
    )
}

export default Loading
