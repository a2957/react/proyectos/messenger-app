import React, { useContext } from 'react'
import { Link } from 'react-router-dom';
import UserContext from '../../context/User/UserContext'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSignOutAlt} from '@fortawesome/free-solid-svg-icons'

function User() {
    
    const {user, LogOut} = useContext(UserContext)

    return (
        <div className="User">
            <div className="Avatar"></div>
            <div className="Information">
            {
                user 
                    ? ( <div className="Profile">
                            <h1>{user.firstName} {user.lastName}</h1>
                            <p>{user.email}</p>
                        </div>) 
                    : <h1>No hay Usuario Loggeado</h1>
            }
            <Link to="/" onClick={async() =>{
                await LogOut();
            }}><FontAwesomeIcon icon={faSignOutAlt} size="2x"/></Link>
            </div>
        </div>
    )
}

export default User
