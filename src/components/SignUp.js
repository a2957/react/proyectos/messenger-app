import React, { useState } from 'react'
import '../css/SignUp.css';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { LogUp } from '../config/firebase';
import { createUser } from '../services/createUser';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faGoogle} from '@fortawesome/free-brands-svg-icons'

function SignUp() {

    const [message, setMessage] = useState('')
    const history = useHistory();

    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const onSubmit = async data => {
        
        if(data.password !== data.confirmPassword){
            setMessage('Las contraseñas no coinciden')
            return 0
        } 

        if(data.email !== data.confirmEmail){
            setMessage('Los correos no coinciden')
            return 0
        }

        const uid = await LogUp(data.email, data.password)

        const user = {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            username: data.username,
            uid: uid,
        }

        console.log(user)
        const response = await createUser(user)
        console.log(response)
        
        reset({
            username: '',
            firtName: '',
            lastName: '',
            email: '',
            confirmEmail: '',
            password: '',
            confirmPassword: ''
        })

        history.push('/signin')
    }
    console.log(errors)

    return (

            <div className="SignUp">  
                <div>{message}</div>
                <h1>Register</h1>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input type="text" placeholder="Username" {...register("username", {required: true})} />
                    <div className="FullName">
                        <input type="text" placeholder="First Name" {...register("firstName", {required: true})} />
                        <input type="text" placeholder="Last Name" {...register("lastName", {required: true})} />
                    </div>
                    <div className="Email">
                        <input type="email" placeholder="Email" {...register("email", {required: true})} />
                        <input type="text" placeholder="Confirm Email" {...register("confirmEmail", {required: true})} />
                    </div>
                    <div className="Password">
                        <input type="password" placeholder="Password" {...register("password", {required: true})} /> 
                        <input type="password" placeholder="Confirm Password" {...register("confirmPassword", {required: true})} /> 
                    </div>
                   
                    <input type="url" placeholder="Photo Url" {...register("photoUrl", {})} />
            
                    <input type="submit" value="Register"/>
                </form>
                <div className="withGoogle">
                <div className="line"></div>
                <div className="text">
                    <h4>or log in with</h4>
                </div>
                <div className="line"></div>
                </div>
                <div className="Google">
                    <button onClick={() => {
                        //TODO: Generar el ingreso por Google
                    }}><FontAwesomeIcon icon={faGoogle}/> <span>Google</span></button>
                </div>

                <div className="footer">
                    <h4>Have an account?</h4>
                    <button onClick={() => {
                        history.push("/signin")
                    }}>Log In!</button>
                </div>
            </div>
    )
}

export default SignUp
