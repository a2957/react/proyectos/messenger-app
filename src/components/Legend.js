import React from 'react'


function Legend() {

    return (
        <div className="Legend">
            <h1>Text App</h1>
            <div className="slogan">
                <h2>Be modern.</h2>
                <h2>Be agile.</h2>
                <h2>Keep it simple</h2>
            </div>
            <p>With TextApp, messaging is simple, fast and secure. Do not set limits to advance. Communicate!</p>

        </div>
    )
}

export default Legend
