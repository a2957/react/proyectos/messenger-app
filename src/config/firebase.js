import {initializeApp} from 'firebase/app'
import {getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut} from 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyDwomNloZTtCeS3hNDFN_fCMwD68hwn_ps",
    authDomain: "messenger-app-react-88884.firebaseapp.com",
    projectId: "messenger-app-react-88884",
    storageBucket: "messenger-app-react-88884.appspot.com",
    messagingSenderId: "52823994060",
    appId: "1:52823994060:web:dbe4a6dc4ee7c306f95c69",
    measurementId: "G-LXR0XRTZHH"
  };


const app  = initializeApp(firebaseConfig)
const auth = getAuth(app)

export const LogIn = async (email, password) =>{
    try {
        const userCredential = await signInWithEmailAndPassword(auth, email, password)
        return userCredential
    } catch (error) {
        console.log(error)
        return error
    }
}

export const LogUp = async (email, password) => {
    try {
        const userCredential = await createUserWithEmailAndPassword(auth, email, password)
        console.log(userCredential)
        return userCredential.user.uid
    } catch (error) {
        console.log(error.code)
        return error
    }
}

export const validateAuth = () => {
    const user = auth.currentUser;
    return user
}

export const LogOut = async () => {
    await signOut(auth)
}
