import './App.css';
import UserState from './context/User/UserState';
import {BrowserRouter as Router} from 'react-router-dom'
import Routes from './routes/Routes'

function App() {
  return (
    <div className="App">
      <UserState>
        <Router >
          <Routes />
        </Router>
      </UserState>
    </div>
  );
}

export default App;
