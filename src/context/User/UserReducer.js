
import {SIGN_IN, SING_OUT} from '../types'

const UserReducer = (state, action) => {
    const {payload, type} = action

    switch (type) {
        case SIGN_IN:
            return {
                ...state,
                user: payload
            }
        case SING_OUT:
            return {
                ...state,
                user: null
            }
        default:
            return state
    }
}

export default UserReducer