import axios from 'axios'
import React, { useReducer } from 'react'
import UserContext from './UserContext'
import UserReducer from './UserReducer'

function UserState(props) {

    const initialState = {
        user: null
    }

    const [state, dispatch] = useReducer(UserReducer, initialState)

    const SignIn = async (id) => {
        const {data} = await axios.get('https://academlo-whats.herokuapp.com/api/v1/users/')
        const user = data.filter( user => user.uid === id)
        dispatch({
            type: 'SIGN_IN', 
            payload: user[0]
        })
    }

    const SignOut = () => {
        dispatch({
            type: 'SING_OUT'
        })
    }

    return (
        <UserContext.Provider value={{
            user: state.user,
            SignIn,
            SignOut
        }}>
            {props.children}
        </UserContext.Provider>
    )
}

export default UserState